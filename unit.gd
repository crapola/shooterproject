extends KinematicBody

const gravity:float=1.0
const VELOCITY:float=3.0
const jump:float=20.0

onready var camera=$Camera
# x=strafe,y=forward,z=up.
var speed:Vector3
# x=yaw,y=pitch in radians.
var orientation:Vector2

func _input(event):
	if event is InputEventKey:
		if event.scancode==KEY_ESCAPE:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		print(transform.basis.x.normalized())
	if event is InputEventMouseMotion:
		mouse_look(event.relative)

func _physics_process(delta):
	# Apply gravity.
	speed.z-=gravity
	# Move.
	var v:Vector3
	v=transform.basis.z.normalized()*-speed.y
	v+=transform.basis.x.normalized()*speed.x
	v.y+=speed.z
	var kc=move_and_slide(v,Vector3(0,1,0))
	if is_on_floor():
		speed.z=-speed.z/2.0
	# Friction.
	speed.x*=0.75
	speed.y*=0.75

func _process(delta):
	var impulse=Vector2()
	if Input.is_action_pressed("ui_page_up") and is_on_floor():
		speed.z=jump
	if Input.is_action_pressed("ui_up"):
		impulse.y=1.0
	if Input.is_action_pressed("ui_down"):
		impulse.y=-1.0
	if Input.is_action_pressed("ui_left"):
		impulse.x=-1.0
	if Input.is_action_pressed("ui_right"):
		impulse.x=1.0
	impulse=impulse.normalized()*VELOCITY
	speed.x+=impulse.x
	speed.y+=impulse.y

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	pass

func mouse_look(delta:Vector2) -> void:
	var ratio:float=0.005
	orientation.x-=delta.x*ratio
	orientation.y-=delta.y*ratio
	orientation.y=clamp(orientation.y,deg2rad(-90),deg2rad(90))
	orientation.x=fmod(orientation.x,deg2rad(360))
	# Reference: https://docs.godotengine.org/en/3.1/tutorials/3d/using_transforms.html
	# Rotate body yaw.
	transform.basis=Basis()
	rotate_object_local(Vector3(0,1,0),orientation.x)
	# Rotate camera pitch.
	camera.transform.basis=Basis()
	camera.rotate_object_local(Vector3(1,0,0),orientation.y)